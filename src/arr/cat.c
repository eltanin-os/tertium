#include <tertium/cpu.h>
#include <tertium/std.h>

ctype_status
c_arr_cat(ctype_arr *p, void *v, usize m, usize n)
{
	if (c_arr_ready(p, m, n) < 0)
		return -1;

	c_mem_cpy(p->p + p->n, m, v);
	p->n += m;
	p->p[p->n] = 0;
	return 0;
}
